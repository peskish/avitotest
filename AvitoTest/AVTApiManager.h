//
//  AVTApiManager.h
//  AvitoTest
//
//  Created by Artem Peskishev on 09.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AVTApiManager : NSObject

+ (AVTApiManager*)sharedInstance;

- (void)getItunesSearchResults:(NSString *)searchQuery
               CompletionBlock:(void (^)(NSArray *answer, NSError *error))completioBlock;

- (void)getGitHubSearchResults:(NSString *)searchQuery
               CompletionBlock:(void (^)(NSArray *answer, NSError *error))completioBlock;
@end
