//
//  AVTApiManager.m
//  AvitoTest
//
//  Created by Artem Peskishev on 09.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "AVTApiManager.h"

#define API_ITUNES_SEARCH_URL @"https://itunes.apple.com/search?"
#define API_GITHUB_SEARCH_USERS_URL @"https://api.github.com/search/users?"


@interface AVTApiManager ()

@property (nonatomic,strong) NSURLSession *session;

@end

@implementation AVTApiManager


+ (AVTApiManager*)sharedInstance {
    
    static AVTApiManager *_sharedInstance = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[AVTApiManager alloc] init];
    });
    return _sharedInstance;
}


-(instancetype)init {
    
    self = [super init];
    if (self)
    {
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        [sessionConfiguration setAllowsCellularAccess:YES];
        [sessionConfiguration setHTTPAdditionalHeaders:@{ @"Accept" : @"application/json" }];
        
        self.session = [NSURLSession sessionWithConfiguration:sessionConfiguration];

    }
    return self;
}


- (void)getItunesSearchResults:(NSString *)searchQuery
               CompletionBlock:(void (^)(NSArray *answer, NSError *error))completioBlock {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

    NSString *searchString = [API_ITUNES_SEARCH_URL stringByAppendingString:[NSString stringWithFormat:@"term=%@",searchQuery]];

    
    NSURL *url = [NSURL URLWithString:searchString];

    
    [[self.session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if (!error)
        {
            NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSArray *resultArray = resultDict[@"results"];
            completioBlock(resultArray,nil);
        }
    }] resume];
}

- (void)getGitHubSearchResults:(NSString *)searchQuery
               CompletionBlock:(void (^)(NSArray *answer, NSError *error))completioBlock {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSString *searchString = [API_GITHUB_SEARCH_USERS_URL stringByAppendingString:[NSString stringWithFormat:@"q=%@",searchQuery]];
    
    NSURL *url = [NSURL URLWithString:searchString];
    
    [[self.session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSArray *resultArray = resultDict[@"items"];
        completioBlock(resultArray,nil);
    }] resume];
}

@end
