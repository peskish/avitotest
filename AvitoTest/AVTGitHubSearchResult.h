//
//  AVTGitHubSearchResult.h
//  AvitoTest
//
//  Created by Artem Peskishev on 09.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "AVTSearchResult.h"

@interface AVTGitHubSearchResult : AVTSearchResult

-(id)initWithDictionary:(NSDictionary *)dict;

@end
