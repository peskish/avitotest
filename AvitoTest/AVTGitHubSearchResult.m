//
//  AVTGitHubSearchResult.m
//  AvitoTest
//
//  Created by Artem Peskishev on 09.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "AVTGitHubSearchResult.h"

@implementation AVTGitHubSearchResult

-(id)initWithDictionary:(NSDictionary *)dict{
    self.imageURL = [NSURL URLWithString:dict[@"avatar_url"]];
    self.authorString = dict[@"html_url"];
    self.title = dict[@"login"];
    return self;
}

@end
