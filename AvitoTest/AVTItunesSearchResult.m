//
//  AVTItunesSearchResult.m
//  AvitoTest
//
//  Created by Artem Peskishev on 09.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "AVTItunesSearchResult.h"

@implementation AVTItunesSearchResult

-(id)initWithDictionary:(NSDictionary *)dict{
    self.imageURL = [NSURL URLWithString:dict[@"artworkUrl100"]];
    self.authorString = dict[@"artistName"];
    self.title = dict[@"trackName"];
    return self;
}

@end
