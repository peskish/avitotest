//
//  AVTSearchManager.h
//  AvitoTest
//
//  Created by Artem Peskishev on 09.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AVTSearchResult.h"

@protocol AVTSearchManagerDelegate
- (void)resultsAvailiable:(NSArray *)resultsArray done:(BOOL)done;
@end

@interface AVTSearchManager : NSObject 

@property (weak) id<AVTSearchManagerDelegate> delegate;

- (instancetype)initWithDelegate:(id<AVTSearchManagerDelegate>) theDelegate;

- (void)getResultsForSearch:(NSString *)searchString WithType:(DataType)dataType;

@end
