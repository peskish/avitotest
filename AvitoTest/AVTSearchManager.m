//
//  AVTSearchManager.m
//  AvitoTest
//
//  Created by Artem Peskishev on 09.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "AVTSearchManager.h"
#import "AVTApiManager.h"

@implementation AVTSearchManager

-(instancetype)initWithDelegate:(id<AVTSearchManagerDelegate>)theDelegate
{
    self = [super init];
    
    if (self) {
        self.delegate = theDelegate;
    }
    return self;
}

- (void)getResultsForSearch:(NSString *)searchString WithType:(DataType)dataType
{
    if (dataType == ItunesType)
    {
        [self getItunesResultsForSearch:searchString];
    } else if (dataType == GitHubType)
    {
        [self getGitHubResultsForSearch:searchString];
    }
}

- (void)getItunesResultsForSearch:(NSString *)searchString
{
    [[AVTApiManager sharedInstance]getItunesSearchResults:searchString CompletionBlock:^(NSArray *answer, NSError *error) {
        
        NSArray *resultArray = [self formResultsArrayFrom:answer WithType:ItunesType];
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [self.delegate resultsAvailiable:resultArray done:YES];
        });
    }];
}

- (void)getGitHubResultsForSearch:(NSString *)searchString
{
    [[AVTApiManager sharedInstance]getGitHubSearchResults:searchString CompletionBlock:^(NSArray *answer, NSError *error) {
        
        NSArray *resultArray = [self formResultsArrayFrom:answer WithType:GitHubType];
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [self.delegate resultsAvailiable:resultArray done:YES];
        });
    }];
}

- (NSArray *)formResultsArrayFrom:(NSArray *)array WithType:(DataType)dataType
{
    NSMutableArray *resultsArray = [NSMutableArray new];
    
    for (NSDictionary *objectDict in array)
    {
        AVTSearchResult *searhResult = [[AVTSearchResult alloc]initWithDictionary:objectDict type:dataType];
        [resultsArray addObject:searhResult];
    }
    
    return resultsArray;
    
}

@end
