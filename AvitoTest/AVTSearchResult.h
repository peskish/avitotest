//
//  AVTSearchResult.h
//  AvitoTest
//
//  Created by Artem Peskishev on 09.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, DataType) {
    ItunesType,
    GitHubType,
};

@protocol AVTSearchResultProtocol
@required
    -(id)initWithDictionary:(NSDictionary *)dict;
@end

@interface AVTSearchResult : NSObject <AVTSearchResultProtocol>

@property (nonatomic) NSURL *imageURL;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *authorString;
@property (nonatomic) UIImage *image;

-(id)initWithDictionary:(NSDictionary *)dict type:(DataType)dataType;

@end
