//
//  AVTSearchResult.m
//  AvitoTest
//
//  Created by Artem Peskishev on 09.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "AVTSearchResult.h"
#import "AVTItunesSearchResult.h"
#import "AVTGitHubSearchResult.h"

@implementation AVTSearchResult

-(id)initWithDictionary:(NSDictionary *)dict type:(DataType)dataType
{
    self = nil;
    if (dataType == ItunesType){
        self = [[AVTItunesSearchResult alloc] initWithDictionary:dict];
    } else if (dataType == GitHubType)
    {
        self = [[AVTGitHubSearchResult alloc]initWithDictionary:dict];
    }
    return self;
}


@end
