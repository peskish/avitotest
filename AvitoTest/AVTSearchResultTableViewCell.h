//
//  AVTSearchResultTableViewCell.h
//  AvitoTest
//
//  Created by Artem Peskishev on 09.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AVTSearchResult.h"

@interface AVTSearchResultTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *thumbImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *desctiptionLabel;
@property (strong, nonatomic) NSIndexPath *indexPath;

- (void)setCell:(AVTSearchResult *)searchResult Index:(NSInteger)index;

- (void)setToLeft;

- (void)setToRight;

@end
