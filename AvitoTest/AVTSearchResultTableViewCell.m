//
//  AVTSearchResultTableViewCell.m
//  AvitoTest
//
//  Created by Artem Peskishev on 09.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "AVTSearchResultTableViewCell.h"

const int SPACER = 8;

@implementation AVTSearchResultTableViewCell

- (void)awakeFromNib {
    self.thumbImageView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *imageTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showImage)];
    
    [self.thumbImageView addGestureRecognizer:imageTap];
}

-(void)setCell:(AVTSearchResult *)searchResult Index:(NSInteger)index
{
    
    if (searchResult.image)
    {
        self.thumbImageView.image = searchResult.image;

    } else {
        self.thumbImageView.image = [UIImage imageNamed:@"placeholder.jpg"];
        NSURLSessionDownloadTask *downloadImageTask = [[NSURLSession sharedSession]
                                                       downloadTaskWithURL:searchResult.imageURL completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                                                           
                                                           UIImage *downloadedImage = [UIImage imageWithData:
                                                                                       [NSData dataWithContentsOfURL:location]];
                                                           dispatch_async(dispatch_get_main_queue(), ^(void) {
                                                               self.thumbImageView.image = downloadedImage;
                                                           });
                                                           searchResult.image = downloadedImage;
                                                       }];
        [downloadImageTask resume];
    }
    
    
    self.titleLabel.text = searchResult.title;
    self.desctiptionLabel.text = searchResult.authorString;
}

-(void)setToLeft
{
    self.thumbImageView.frame = CGRectMake(SPACER, 0, self.thumbImageView.frame.size.width, self.thumbImageView.frame.size.height);
    
    self.thumbImageView.center = CGPointMake(self.thumbImageView.center.x, self.frame.size.height/2);
    
    self.titleLabel.frame = CGRectMake(self.thumbImageView.frame.origin.x + self.thumbImageView.frame.size.width + SPACER, self.thumbImageView.frame.origin.y, self.frame.size.width - self.thumbImageView.frame.size.width - SPACER*3, self.titleLabel.frame.size.height);
    
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    
    self.desctiptionLabel.frame = CGRectMake(self.titleLabel.frame.origin.x, self.titleLabel.frame.origin.y + self.titleLabel.frame.size.height,self.titleLabel.frame.size.width, self.desctiptionLabel.frame.size.height);
    
    self.desctiptionLabel.textAlignment = NSTextAlignmentLeft;
}

-(void)setToRight
{
    self.thumbImageView.frame = CGRectMake(self.frame.size.width - self.thumbImageView.frame.size.width - SPACER, 0, self.thumbImageView.frame.size.width, self.thumbImageView.frame.size.height);
    
    self.thumbImageView.center = CGPointMake(self.thumbImageView.center.x, self.frame.size.height/2);
    
    self.titleLabel.frame = CGRectMake(SPACER, self.thumbImageView.frame.origin.y, self.frame.size.width - self.thumbImageView.frame.size.width - SPACER*3, self.titleLabel.frame.size.height);
    
    self.titleLabel.textAlignment = NSTextAlignmentRight;
    
    self.desctiptionLabel.frame = CGRectMake(self.titleLabel.frame.origin.x, self.titleLabel.frame.origin.y+self.titleLabel.frame.size.height,self.titleLabel.frame.size.width, self.desctiptionLabel.frame.size.height);
    
    self.desctiptionLabel.textAlignment = NSTextAlignmentRight;
}

- (void)showImage
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"showLargeImage" object:self.indexPath];
}
@end
