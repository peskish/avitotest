//
//  ViewController.h
//  AvitoTest
//
//  Created by Artem Peskishev on 09.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AVTSearchManager.h"

@interface ViewController : UIViewController <AVTSearchManagerDelegate,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>


@end

