//
//  ViewController.m
//  AvitoTest
//
//  Created by Artem Peskishev on 09.02.16.
//  Copyright © 2016 Artem Peskishev. All rights reserved.
//

#import "ViewController.h"
#import "AVTSearchResultTableViewCell.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSArray *searchItemsArray;
@property AVTSearchManager *searchManager;
@property NSString *searchString;
@property (strong, nonatomic) UISegmentedControl *sourceFilter;

@property NSOperationQueue *operationQueue;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (strong, nonatomic) UIImageView *largeImageFromCell;
@property (strong, nonatomic) IBOutlet UIView *blackView;
@property (weak, nonatomic) IBOutlet UILabel *noContentLabel;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property CGRect oldFrame;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(animatedShowImage:) name:@"showLargeImage" object:nil];
    
    self.sourceFilter = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"iTunes", @"GitHub", nil]];
    [self.sourceFilter sizeToFit];
    self.navigationItem.titleView = self.sourceFilter;
    self.sourceFilter.selectedSegmentIndex = 0;
    [self.sourceFilter addTarget:self action:@selector(handleSegmentControl:) forControlEvents: UIControlEventValueChanged];
    
    self.searchManager = [[AVTSearchManager alloc]initWithDelegate:self];
    
    self.operationQueue = [NSOperationQueue new];
    self.operationQueue.maxConcurrentOperationCount = 1;
    
    [self.searchBar becomeFirstResponder];
    
    self.largeImageFromCell = [[UIImageView alloc]init];
    self.largeImageFromCell.contentMode = UIViewContentModeScaleAspectFit;
    self.largeImageFromCell.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideLargeImage)];
    [self.largeImageFromCell addGestureRecognizer:tap];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:self.activityIndicator];
    [self navigationItem].rightBarButtonItem = barButton;
    self.activityIndicator.hidesWhenStopped = YES;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"showLargeImage" object:nil];
}

-(void)handleSegmentControl:(UISegmentedControl *)segmentedControl
{
    self.searchItemsArray = nil;
    [self.tableView reloadData];
    [self startSearch];
}

-(void)resultsAvailiable:(NSArray *)resultsArray done:(BOOL)done
{
    [self.activityIndicator stopAnimating];
    self.searchItemsArray = resultsArray;
    [self.tableView reloadData];
}


-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    self.searchString = self.searchBar.text;
    [self startSearch];
    [self.view endEditing:YES];
}

-(void)startSearch
{
    self.noContentLabel.hidden = NO;
    [self.activityIndicator startAnimating];
    switch (self.sourceFilter.selectedSegmentIndex) {
        case 0:
            [self.searchManager getResultsForSearch:self.searchString WithType:ItunesType];
            break;
        case 1:
            [self.searchManager getResultsForSearch:self.searchString WithType:GitHubType];
            break;
        default:
            break;
    }
}


//========================--------------------
#pragma mark - TableView
//========================--------------------

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    self.tableView.hidden = NO;

    if (!self.searchItemsArray.count)
    {
        [self showEmptyTable];
    }
    return self.searchItemsArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SearchResultItemCell";
    
    
    AVTSearchResultTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
    
    [cell setCell:self.searchItemsArray[indexPath.row] Index:indexPath.row];
    
    BOOL isEven = (indexPath.row+1) % 2;
    
    cell.indexPath = indexPath;
    
    if (((isEven)&&(![self.sourceFilter selectedSegmentIndex]))||((!isEven)&&([self.sourceFilter selectedSegmentIndex])))
    {
        [cell setToLeft];
    } else {
        [cell setToRight];
    }
    
    return cell;
}

-(void)showEmptyTable
{
    self.tableView.hidden = YES;
    self.noContentLabel.hidden = NO;
    if (self.searchString.length)
    {
        [self.noContentLabel setText:@"Ничего не найдено"];
    } else {
        [self.noContentLabel setText:@"Введите запрос в строку поиска"];
    }
    
    [self.noContentLabel sizeToFit];
    self.noContentLabel.center = self.tableView.center;
}

//========================--------------------
#pragma mark - Keyboard Notifications
//========================--------------------

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGRect rect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    self.tableView.frame = CGRectMake(0, self.searchBar.frame.size.height + self.searchBar.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height - rect.size.height - self.searchBar.frame.size.height - self.searchBar.frame.origin.y);
    self.noContentLabel.center = self.tableView.center;

    [self.view layoutSubviews];
}

-(void)keyboardWillHide:(NSNotification *)notification
{

    self.tableView.frame = CGRectMake(0, self.searchBar.frame.size.height + self.searchBar.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height - self.searchBar.frame.size.height - self.searchBar.frame.origin.y);
    self.noContentLabel.center = self.tableView.center;
    [self.view layoutSubviews];
    
}

//========================--------------------
#pragma mark - Show large image
//========================--------------------

- (void)animatedShowImage:(NSNotification *)notification
{
    [self.view endEditing:YES];
    
    NSIndexPath *indexPath = notification.object;
    AVTSearchResult *searchResult = self.searchItemsArray[indexPath.row];

    AVTSearchResultTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];

    CGRect rectOfCellInTableView = [self.tableView rectForRowAtIndexPath:indexPath];
    CGRect rectOfCellInSuperview = [self.tableView convertRect:rectOfCellInTableView toView:self.view];
    CGRect rectOfImageViewInCell = CGRectMake(cell.thumbImageView.frame.origin.x, cell.thumbImageView.frame.origin.y + rectOfCellInSuperview.origin.y, cell.thumbImageView.frame.size.width, cell.thumbImageView.frame.size.height);
    
    self.oldFrame = rectOfImageViewInCell;
    
    self.largeImageFromCell.image = searchResult.image;
    self.largeImageFromCell.frame = rectOfImageViewInCell;
    
    UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
    self.blackView.alpha = 0;
    [self.blackView addSubview:self.largeImageFromCell];
    self.blackView.frame = currentWindow.bounds;
    [currentWindow addSubview:self.blackView];
    
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationCurveEaseIn
                     animations:^ {
                         CGRect frame = self.view.frame;
                         self.largeImageFromCell.frame = frame;
                         self.blackView.alpha = 1;
                         
                     } completion:^( BOOL completed ) {
                     }];
}

-(void)hideLargeImage
{
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationCurveEaseIn
                     animations:^ {
                         CGRect frame = self.oldFrame;
                         self.largeImageFromCell.frame = frame;
                         self.blackView.alpha = 0;
                     } completion:^( BOOL completed ) {
                         [self.blackView removeFromSuperview];
                     }];
}
@end
